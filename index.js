#!/usr/bin/env node

const argv = require('minimist')(process.argv.slice(2))
const chalk = require('chalk')
const NeocitiesApi = require('neocities')

if (Object.keys(argv).length === 1) {
  console.log('Usage: neocities-cli --<argument>=<value>')
  console.log('')
  console.log('Arguments:')
  console.log('  --auth=<file>', '          Use an auth file. Defaults to ./neocities.config.js')
  console.log('  --username=<username>', '  Your username.')
  console.log('  --password=<password>', '  Your password.')
  console.log('  --upload=<file|dir>', '    Uploads a file or directory.')
  console.log('  --delete=<files>', '       Deletes files, comma separated.')
  console.log('  --info', '                 Get information about your site.')
  console.log('  --json', '                 Return result in JSON format.')
  return
}

function success (data) {
  if (argv.json) {
    console.log(JSON.stringify(data))
  } else {
    if (data instanceof Array) {
      data.forEach((v, i) => {
        console.log(v)
      })
      console.log(chalk`{black {bgYellow success}}`)
    } else if (typeof data === 'object') {
      let longestKey = 0
      for (let k in data) {
        if (k.length > longestKey) longestKey = k.length
      }
      for (let k in data) {
        let keyName = k.toLowerCase()
        while (keyName.length < longestKey) {
          keyName = ' '+keyName
        }
        console.log(chalk`{black {bgYellow ${keyName}}} ${data[k]}`)
      }
    } else {
      console.log(chalk`{black {bgYellow success}} ${data}`)
    }
  }
}

function error (msg) {
  console.log(chalk`{black {bgRed error}} ${msg}`)
}

// Authentication

const setup = {}

if (argv.auth) {
  try {
    const path = typeof argv.auth === 'string' ? argv.auth : './neocities.config.js'
    const auth = require(`${process.cwd()}/${path}`)
    if (!auth.username) return error(`Auth file missing username field.`)
    if (!auth.password) return error(`Auth file missing password field.`)
    setup.username = auth.username
    setup.password = auth.password
  } catch (err) {
    return error(err.message)
  }
} else {
  if (!argv.username) return error(`Missing username parameter.`)
  if (!argv.password) return error(`Missing password parameter.`)
  setup.username = argv.username
  setup.password = argv.password
}

const api = new NeocitiesApi(setup.username, setup.password)

// API

if (argv.upload) {
  const fs = require('fs')
  const path = argv.upload

  // Check file type

  let type = undefined

  try {
    const stats = fs.lstatSync(path)
    if (stats.isFile()) type = 'file'
    else if (stats.isDirectory()) type = 'directory'
    else errorMessage = 'Invalid file or directory type.'
  } catch (err) {
    return error(err.message)
  }

  // Get files

  const files = []

  try {
    if (type === 'file') {
      files.push({ name: path, path: path })
    } else {
      function getFiles (path) {
        fs
          .readdirSync(path)
          .forEach((file) => {
            const stats = fs.lstatSync(`${path}/${file}`)
            if (stats.isFile()) files.push({ name: `${path}/${file}`, path: `${path}/${file}` })
            else if (stats.isDirectory()) getFiles(`${path}/${file}`)
          })
      }
      getFiles(path)
    }
  } catch (err) {
    return error(err.message)
  }

  console.log('Uploading...')
  files.forEach((file) => console.log(file.path))

  // Do upload

  api.upload(files, (res) => {
    if (res.result === 'error') return error(res.message)
    success(res.message)
  })
}

if (argv.delete) {
  const files = argv.delete.split(',')

  api.upload(files, (res) => {
    if (res.result === 'error') return error(res.message)
    success(res.message)
  })
}

if (argv.info) {
  api.info((res) => {
    if (res.result === 'error') return error(res.message)
    success(res.info)
  })
}
